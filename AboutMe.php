<?php

class People{
    protected $username;
    protected $age;
    protected $gender;
    protected $education;
    protected $repository;
    public $birthday;
    public $city;

    public function __construct($birthday = null, $city = 'Київ'){
        $this->username = 'Богдан Кісарін';
        $this->gender = 'чоловіча';
        $this->education = ['title' => 'Запорізький національний технічний університет', 'specialty' => "Комп'ютерні системи та мережі", 'begin' => 2004, 'end' => 2009];
        $this->repository = 'Упс...додам лінкі згодом';
        $this->city = $city;
        ($birthday) ? $this->birthday = strtotime($birthday) : $this->birthday = 'вік не заданий';
        $this->age = $this->getAge();
    }

    public function getAge(){
        if(!is_string($this->birthday)){
            $currentTime = time();
            // грубий підрахунок - пруф https://habrahabr.ru/sandbox/81171/, 31556926 - кількість секунд в році
            $this->age = (int)(( $currentTime - $this->birthday )/31556926);

            return $this->age;
        }
        else {
            return $this->age = $this->birthday;
        }
    }

    public function getMyHometown(){
        return $city = 'Запоріжжя';
    }

}

class Worker extends People{

    public $skills;

    public function __construct($addskills = null, $birthday = null) {

        parent::__construct($birthday);

        $this->skills = [
            0 => 'Знання та досвід використання: php, mysql, js, jSomething is wrong, html/css',
            1 => 'Досвід роботи з фреймворками: Yii, Yii2',
            2 => 'CMS: Opencart',
            3 => 'GitLab, Bitbucket, GitHub',
            4 => 'Знаю лячну абревіатуру ООП та те, що ховається під нею'
        ];

        if($addskills) {
            $this->addSkills($addskills);
        }
    }

    public function addSkills($addskills){
        if($addskills){
            array_push($this->skills, $addskills);

        }
    }

    public function getSkills(){

        return $this->skills;

    }

    public function addHtml($skills){

        $result = null;
        $result .= '<ul>';
        foreach($skills as $skill){
            $result .= '<li>' . $skill . '</li>';
        }

        $result .= '</ul>';

        return $result;

    }

    public function getShortDataAbouMe(){
        $info = 'Привіт! Мене звати '.$this->username.' і я маю нагоду трішки розказати про себе.
		Мені '. $this->age . ' років, народився в місті ' . $this->getMyHometown() . ', але наразі проживаю в місті '.$this->city.'. 
		Моя альма-матер - це ' . $this->education['title'] . ', де я навчався з ' . $this->education['begin'] . ' по ' . $this->education['end'] .
            ' рік за спеціальністю "'.$this->education['specialty'].'". Повний шлях мене, як працівника описаний в резюме. Зараз працюю джуном 
		в невеличкій веб-студії і шукаю місце, де міг би більше себе реалізувати і приумножити знання та навики. 
		Думаю Вас зацікавить переглянути мій код на репозиторії: '. $this->repository. '. Наразі про себе можу сказати наступне:';
        echo $info;

    }

}

$worker = new Worker('Вмію гуглити :)', '28.08.1987');
echo $worker->getShortDataAbouMe();
echo $worker->addSkills('адекватно сприймаю критику');
echo $worker->addHtml($worker->getSkills());